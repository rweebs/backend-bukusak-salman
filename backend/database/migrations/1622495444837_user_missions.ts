import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UserMissions extends BaseSchema {
  protected tableName = 'user_missions'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamps(true,true)
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('mission_id').unsigned().references('id').inTable('missions').onDelete('CASCADE')
      table.boolean('is_completed')
      table.boolean('is_validated')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
