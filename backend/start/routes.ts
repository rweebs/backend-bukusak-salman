/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

Route.post('/api/register','AuthController.register')

Route.post('/api/login','AuthController.login')
Route.get('/api/user/data','AuthController.showUser')
Route.get('/api/user/level','AuthController.showLevel')
Route.post('/api/mission/create','MissionsController.create')

Route.get('/api/mission/available','MissionsController.showMissionAvailable')

Route.get('/api/mission/active','MissionsController.showMissionActive')

Route.post('/api/mission/completed','MissionsController.markAsCompleted')

Route.post('/api/mission/validated','MissionsController.markAsValidated')

Route.put('/api/mission/edit','MissionsController.editMission')

Route.delete('/api/mission/delete','MissionsController.deleteMission')

Route.get('/api/admin/pendings','MissionsController.showPendings')

Route.get('/api/admin/users','MissionsController.showUserAll')

Route.get('/api/admin/missions','MissionsController.showMissionAll')


