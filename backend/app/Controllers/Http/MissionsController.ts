import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {schema} from '@ioc:Adonis/Core/Validator'
import Mission from "App/Models/Mission"
import Database from '@ioc:Adonis/Lucid/Database'


export default class MissionsController {
    public async create({auth,response,request}:HttpContextContract){
        const validator = schema.create({
            name: schema.string(),
            poin :schema.number(),
            level:schema.number(),
            description:schema.string()
        });
        const body= await request.all()
        const validated = await request.validate({
            schema: validator
        });
        const user = await auth.authenticate();

        if (!user || !user.is_admin) return response.unauthorized({
            success: false,
            messsage: "unauthorized"
        });
        else{
        const mission = new Mission();
        mission.name = validated.name;
        mission.poin = validated.poin;
        mission.level = validated.level;
        mission.description = validated.description;
        if (body.is_available){
            mission.is_available=body.is_available
        }

        await mission.save();
        return response.created({
            success: true,
            data: mission
        })

}
    }
    public async showMissionAvailable({auth,response}:HttpContextContract){
        const user=await auth.authenticate()
        const poin=await Database.rawQuery(`select poin from users where id=${user.id}`)
        let level=1
        if(poin >=20){
            level=3
        }
        else if(poin >=10){
            level=2
        }
        else{
            level=1
        }
        const data=await Database.rawQuery(`select id,name,description,level,poin from missions where (level<=${level}) & is_available=1;`)
        if (data){
        
        return response.ok({
            success:true,
            data:data[0]
        })}
        else{
            return response.badRequest()
        }

    }

    public async showMissionActive({auth,response}:HttpContextContract){
        const user=await auth.authenticate()
        const poin=await Database.rawQuery(`select poin from users where id=${user.id}`)
        let level=1
        if(poin >=20){
            level=3
        }
        else if(poin >=10){
            level=2
        }
        else{
            level=1
        }
        const data = await Database.rawQuery(`select id,name,description,level,poin from missions where (level<=${level}) & (is_available=1) & (id not in (select missions.id from missions inner join user_missions on missions.id=user_missions.mission_id where (level<=${level}) & (is_available=1) & (user_id=${user.id}) & (is_validated=true)));`)
        if (data){
        
        return response.ok({
            success:true,
            data:data[0]
        })}
        else{
            return response.badRequest()
        }

    }
    public async markAsCompleted({request,auth,response}:HttpContextContract){
        const user=await auth.authenticate()
        const validator = schema.create({
            mission_id:schema.number()
        });
        const validated = await request.validate({
            schema: validator
        });
        const mission = await Mission.findByOrFail('id', validated.mission_id);
        
        if (mission){
            await Database.rawQuery(`insert into user_missions (user_id,mission_id,is_completed,is_validated) values (${user.id},${mission.id},true,false) ;`)

        return response.ok({
            success:true,
        })}
        else{
            return response.badRequest()
        }

    }
    public async markAsValidated({request,auth,response}:HttpContextContract){
        const user=await auth.authenticate()
        const validator = schema.create({
            mission_id:schema.number(),
            user_id:schema.number()
        });
        const body = await request.validate({
            schema: validator
        });
        if (!user || !user.is_admin) return response.unauthorized({
            success: false,
            messsage: "unauthorized"
        })
        else{
        const mission = await Mission.findByOrFail('id', body.mission_id);
        if (mission){
            const passed=await Database.rawQuery(`select * from user_missions where user_id=${body.user_id} & mission_id=${body.mission_id} & is_completed=true;`)
        
            if (passed){
                await Database.rawQuery(`update user_missions set is_validated=true where (user_id=${body.user_id}) & (mission_id =${body.mission_id}) ;`)
                await Database.rawQuery(`update users set poin=poin+ (select poin from missions where id =${body.mission_id}) where id=${body.user_id};`)
            return response.ok({
                success:true,
            })}
            else{
                return response.badRequest()
            }

        }
        else{
            return response.badRequest({
                message:`${body.mission_id} not found`
            })
        }
        
        }
        
    }
    public async showPendings({auth,response}:HttpContextContract){
        const user=await auth.authenticate()
        if (!user || !user.is_admin) return response.unauthorized({
            success: false,
            messsage: "unauthorized"
        });
        else{
            const data=await Database.rawQuery(`select user_id,email,mission_id from users inner join user_missions on user_missions.user_id=users.id where is_validated=false`)
            return response.ok({
                message:"success",
                data:data[0]
            })
        }

    }

    public async editMission({auth,response,request}:HttpContextContract){
        const user=await auth.authenticate()
        const body=await request.all()
        const validator = schema.create({
            mission_id:schema.number()
        });
        const validated = await request.validate({
            schema: validator
        });

        if (!user || !user.is_admin) return response.unauthorized({
            success: false,
            messsage: "unauthorized"
        });
        else{
            if(body.name){
                await Database.rawQuery(`update missions set name = "${body.name}" where id=${validated.mission_id} ;`)
            }
            if(body.description){
                await Database.rawQuery(`update missions set description = "${body.description}" where id=${validated.mission_id} ;`)
            }
            if(body.poin){
                await Database.rawQuery(`update missions set poin = ${body.poin} where id=${validated.mission_id} ;`)
            }
            if(body.level){
                await Database.rawQuery(`update missions set level = ${body.level} where id=${validated.mission_id} ;`)
            }
            return response.ok({
                message:"success"
            })
        }

    }

    public async deleteMission({auth,response,request}:HttpContextContract){
        const user=await auth.authenticate()
        const validator = schema.create({
            mission_id:schema.number()
        });
        const validated = await request.validate({
            schema: validator
        });

        if (!user || !user.is_admin) return response.unauthorized({
            success: false,
            messsage: "unauthorized"
        });
        else{
            
            await Database.rawQuery(`update missions set is_available = false where id=${validated.mission_id} ;`)
        
            return response.ok({
                message:"success"
            })
        }

    }

    public async showUserAll({auth,response}:HttpContextContract){
        const user=await auth.authenticate()
        if (!user || !user.is_admin) return response.unauthorized({
            success: false,
            messsage: "unauthorized"
        });
        else{
            const data=await Database.rawQuery(`select email,name,poin,is_admin from users;`)
        
            return response.ok({
                message:"success",
                data:data[0]
            })
        }

    }

    public async showMissionAll({auth,response}:HttpContextContract){
        const user=await auth.authenticate()
        if (!user || !user.is_admin) return response.unauthorized({
            success: false,
            messsage: "unauthorized"
        });
        else{
            const data=await Database.rawQuery(`select id,name,description,poin,level,is_available from missions;`)
        
            return response.ok({
                message:"success",
                data:data[0]
            })
        }

    }


    
    
    
}
