import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {schema,rules} from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'
import Database from '@ioc:Adonis/Lucid/Database'
export default class AuthController {
    public async register({request,response}:HttpContextContract){
    const body =request.all()
    const validator = {
        schema: schema.create({
            email:schema.string({},[
                rules.required(),
                rules.email(),
                rules.unique({table: 'users', column: 'email'})
            ]),
            password:schema.string({},[
                rules.minLength(6),

            ]),
            name:schema.string({},[
                rules.required()
            ]),

            

        }),
        message:{
            'email.unique':'The email has already been taken.',
            'password.minLength':"The password must be at least 6 characters."
        }
    }
    const validated = await request.validate(validator)
    const newUser = new User();
    newUser.email=validated.email
    newUser.password=validated.password
    newUser.name=validated.name
    if (body.is_admin) {
        newUser.is_admin=body.is_admin
    }
    else{
        newUser.is_admin=false
    }
    await newUser.save()
    return response.status(200).json({
        success: true,
        data:newUser
    });
    }

    public async login({request,auth,response}:HttpContextContract){
        const validator = {
            schema: schema.create({
                email:schema.string({},[
                    rules.required(),
                    rules.email()
                ]),
                password:schema.string({},[
                    rules.minLength(6),
    
                ]),
    
            }),
            messages:{
                'password.minLength':"The password must be at least 6 characters."
            }
        }
        const validated = await request.validate(validator)
        
        try {
            const token = await auth.use('api').attempt(validated.email, validated.password);
            const is_admin=await User.findBy('email',validated.email)
            return response.status(200).json({
                success: true,
                token: token.token,
                is_admin:is_admin?.is_admin
            });
        } catch(err) {
            return response.status(401).json({
                success: false,
                message: "Invalid email or password"
            })
        }

        }
    
        public async showUser({auth,response}:HttpContextContract){
            const user=await auth.authenticate()
            const data=await Database.rawQuery(`select name,email,poin from users where id=${user.id}`)
            
            return response.ok({
                message:"success",
                data:data[0]
            })

        }
        public async showLevel({auth,response}:HttpContextContract){
            const user=await auth.authenticate()
            const data=await Database.rawQuery(`select poin from users where id=${user.id}`)
            let level=0
            if(data.poin >=20){
                level=3
            }
            else if(data.poin>=10){
                level=2
            }
            else{
                level=1
            }
            return response.ok({
                message:"success",
                level:level
            })

        }
    
}
