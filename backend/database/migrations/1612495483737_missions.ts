import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Missions extends BaseSchema {
  protected tableName = 'missions'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamps(true,true)
      table.string('name')
      table.integer('poin')
      table.integer('level')
      table.string('description')
      table.boolean('is_available').defaultTo(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
