import { DateTime } from 'luxon'
import { BaseModel, column,
manyToMany,
ManyToMany } from '@ioc:Adonis/Lucid/Orm'
import User from './User'

export default class Mission extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column()
  public name:string

  @column()
  public poin:number

  @column()
  public level:number

  @column()
  public description:string

  @column()
  public is_available:boolean

  @manyToMany(() =>  User, {
    pivotTable: 'user_missions'
  })
  public bookings: ManyToMany<typeof User>
}
